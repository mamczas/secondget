﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondGetParser
{
	public class ListOfDifferences
	{
		public List<Difference> differences { get; set; }

		public ListOfDifferences()
		{

			differences = new List<Difference>();
		}

		public void AddDifference(Difference diff)
		{

			var item = differences.SingleOrDefault(
				x => x.ContextName == diff.ContextName && x.DatasourceName == diff.DatasourceName);

			if (item != null)
			{
				item.Count += 1;
				item.AdditionalInfo += diff.AdditionalInfo;
			}
			else
			{
				differences.Add(diff);
			}

		}

	}
}
