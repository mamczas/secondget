﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondGetParser
{
	public class Difference
	{
	    public Difference()
	    {
	        Count = 1;
	    }

		public string ContextName { get; set; }
		public string DatasourceName { get; set; }

		public int Count { get; set; }

		public string Fields { get; set; }

		public string AdditionalInfo { get; set; }
	}
}
