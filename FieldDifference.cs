﻿using Newtonsoft.Json;

namespace SecondGetParser
{
    internal class FieldDifference
    {
        public string WithoutGet { get; set; }

        public string WithGet { get; set; }

        public string PropertyName { get; set; }

        public string FieldName { get; set; }

        public string ModifiedValueWithoutGet { get; set; }

        public string ModifiedValueWithGet { get; set; }

        public string ValueWithoutGet { get; set; }

        public string ValueWithGet { get; set; }

        public FieldDifference(string fieldName, string withoutGet, string withGet)
        {
            FieldName = fieldName;
            WithoutGet = withoutGet;
            WithGet = withGet;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
