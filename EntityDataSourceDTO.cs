//© 2015 Fenergo Limited.  Confidential - Receipt and Use Only Via Confidentiality and Non-Disclosure Agreement.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace SecondGetParser
{
    [Serializable]
    [XmlRoot("dataSource")]
    public class EntityDataSourceDTO
    {
        [XmlIgnore]
        public string DataSourceName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("sourceObjectName")]
        public string SourceObjectName { get; set; }

        [XmlAttribute("pKEntityDataFieldKey")]
        public string PKEntityDataFieldKey { get; set; }

        [XmlAttribute("changesetEntityFieldKey")]
        public string ChangesetEntityFieldKey { get; set; }

        [XmlAttribute("breKey")]
        public string BREKey { get; set; }

        [XmlAttribute("authorisationEntityType")]
        public string AuthorisationEntityType { get; set; }

        [XmlAttribute("authorisationEntitySubType")]
        public string AuthorisationEntitySubType { get; set; }

        [XmlAttribute("authorisationEntitySubTypeField")]
        public string AuthorisationEntitySubTypeField { get; set; }

        [XmlAttribute("enableHardDelete")]
        public bool EnableHardDelete { get; set; }

        [XmlAttribute("disableIndexUpdate")]
        public bool DisableIndexUpdate { get; set; }

        [XmlArray("dataFields"), XmlArrayItem("dataField")]
        public List<EntityDataFieldDTO> EntityDataFieldList { get; set; }

        #region Read Only
        [XmlIgnore]
        public bool ReadOnly
        {
            get
            {
                var ro = false;
                if (bool.TryParse(ReadOnlyStr, out ro)) return ro;
                return false;
            }
            set { ReadOnlyStr = Convert.ToString(value); }
        }

        [XmlAttribute("readOnly")]
        public string ReadOnlyStr { get; set; }
        #endregion

        #region Enable Insert
        [XmlIgnore]
        public bool EnableInsert
        {
            get
            {
                var ro = false; if (bool.TryParse(EnableInsertStr, out ro)) return ro;
                return true;
            }
            set { EnableInsertStr = Convert.ToString(value); }
        }

        [XmlAttribute("enableInsert")]
        public string EnableInsertStr { get; set; }
        #endregion

        #region Enable Update
        [XmlIgnore]
        public bool EnableUpdate
        {
            get
            {
                var ro = false; if (bool.TryParse(EnableUpdateStr, out ro)) return ro;
                return true;
            }
            set { EnableUpdateStr = Convert.ToString(value); }
        }

        [XmlAttribute("enableUpdate")]
        public string EnableUpdateStr { get; set; }
        #endregion

        #region Enable Delete
        [XmlIgnore]
        public bool EnableDelete
        {
            get
            {
                var ro = true; if (bool.TryParse(EnableDeleteStr, out ro)) return ro;
                return true;
            }
            set { EnableDeleteStr = Convert.ToString(value); }
        }

        [XmlAttribute("enableDelete")]
        public string EnableDeleteStr { get; set; }
        #endregion

        #region UseContextId
        [XmlIgnore]
        public bool UseContextId
        {
            get
            {
                var ro = false; if (bool.TryParse(UseContextIdStr, out ro)) return ro;
                return false;
            }
            set { UseContextIdStr = Convert.ToString(value); }
        }

        [XmlAttribute("useContextId")]
        public string UseContextIdStr { get; set; }
        #endregion

        #region IsParentBusinessContext used on DataControl to set ContextId from ParentNavigationContext instead of CurrentNavigationContext
        [XmlIgnore]
        public bool IsParentBusinessContext
        {
            get
            {
                var pc = false; if (bool.TryParse(IsParentBusinessContextStr, out pc)) return pc;
                return false;
            }
            set { IsParentBusinessContextStr = Convert.ToString(value); }
        }

        [XmlAttribute("isParentBusinessContext")]
        public string IsParentBusinessContextStr { get; set; }
        #endregion

        #region IsCustomBusinessContext used on DataControl to set ContextId from Behaviours
        [XmlIgnore]
        public bool IsCustomBusinessContext
        {
            get
            {
                var pc = false; if (bool.TryParse(IsCustomBusinessContextStr, out pc)) return pc;
                return false;
            }
            set { IsCustomBusinessContextStr = Convert.ToString(value); }
        }

        [XmlAttribute("isCustomBusinessContext")]
        public string IsCustomBusinessContextStr { get; set; }
        #endregion

        [XmlIgnore]
        public short SelectOrder { get; set; }

        [XmlIgnore]
        public short SaveOrder { get; set; }

        [XmlIgnore]
        public int EntityDataSourceId { get; set; }

        [XmlIgnore]
        public bool NewRecord { get; set; }

        [XmlIgnore]
        public EntityDataFieldDTO PrimaryKey
        {
            get
            {
                return EntityDataFieldList.SingleOrDefault(x => x.Key == PKEntityDataFieldKey);
            }
        }

        private IDictionary<string, object> _instance = new Dictionary<string, object>();
        [XmlIgnore]
        public IDictionary<string, object> ContextParameters
        {
            get { return _instance; }
            set { _instance = value; }
        }

        public override string ToString()
        {
            return $"Name:{DataSourceName}, Source:{SourceObjectName}, ItemCount:{EntityDataFieldList?.Count}";
        }
    }
}
