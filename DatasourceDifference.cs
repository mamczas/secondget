﻿using System.Collections.Generic;
using System.Linq;

namespace SecondGetParser
{
    internal class DatasourceDifference
    {

        public string DifferentFields { get; set; }

        public string DifferenceValues { get; set; }

        public IList<FieldDifference> FieldDifferencesList { get; set; }

        public DatasourceDifference(IList<string> differenceFields, IList<string> differenceValues, IList<FieldDifference> fieldDifferencesList)
        {

            DifferentFields = string.Join(",", differenceFields.Distinct());
            DifferenceValues = string.Join("|", differenceValues.Distinct());
            FieldDifferencesList = fieldDifferencesList.Distinct(new FieldDifferenceComparer()).ToList();
        }
    }
}
