//© 2015 Fenergo Limited.  Confidential - Receipt and Use Only Via Confidentiality and Non-Disclosure Agreement.
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SecondGetParser
{
    [Serializable]
    public class EntityDataFieldDTO
    {
        [XmlAttribute("key")]
        public string Key { get; set; }

        [XmlAttribute("sourceFieldName")]
        public string SourceFieldName { get; set; }

        [XmlAttribute("dataType")]
        public string DataType { get; set; }

        [XmlAttribute("readOnly")]
        public bool ReadOnly { get; set; }

        [XmlAttribute("breLookupName")]
        public string BRELookupName { get; set; }

        [XmlAttribute("breKey")]
        public string BREKey { get; set; }

        [XmlAttribute("breParentKey")]
        public string BREParentKey { get; set; }

        [XmlAttribute("ignored")]
        public bool Ignored { get; set; }
        
        [XmlAttribute("lookupName")]
        public string LookupName { get; set; }

        [XmlAttribute("ignoreDuringErasure")]
        public bool IgnoreDuringErasure { get; set; }

        [XmlAttribute("defaultOnInsert")]
        public string DefaultOnInsert { get; set; }

        [XmlAttribute("defaultOnDelete")]
        public string DefaultOnDelete { get; set; }
        
        /// <summary>
        /// Custom name when creating new field with lookup value for the Id
        /// </summary>
        [XmlAttribute("customLookupBindingKey")]
        public string CustomLookupBindingKey { get; set; }

        #region Multiselect configuration
        [XmlAttribute("multiselectTableName")]
        public string MultiselectTableName { get; set; }

        [XmlAttribute("multiSelectPkColumnName")]
        public string MultiSelectPkColumnName { get; set; }

        [XmlAttribute("multiSelectValueColumnName")]
        public string MultiSelectValueColumnName { get; set; }
        #endregion

        #region IsFKField
        [XmlAttribute("isFK")]
        public string IsFKStr { get; set; }

        [XmlIgnore]
        public bool IsFKField { get; set; }
        #endregion

        #region isConcurrencyField
        [XmlAttribute("isConcurrency")]
        public string IsConcurrencyFieldStr { get; set; }

        [XmlIgnore]
        public bool IsConcurrencyField { get; set; }
		#endregion

		#region Value
		private object _value;
        [XmlIgnore]
        public object Value
        {
            get
            {
                if (_value != null) return _value;
                return GetValue();
            }
            set
            {
                _value = value;
                SetValue(value);
            }
        }
        #endregion

        #region ModifiedValue
        private object _modifiedValue;
        [XmlIgnore]
        public object ModifiedValue
        {
            get
            {
                return _modifiedValue;
            }
            set
            {
                _modifiedValue = value;
            }
        }
        #endregion

        #region DisplayValue
        [XmlIgnore]
        public string DisplayValue
        {
            get
            {
                if (_displayValue == null)
                {
                    if (Value == null)
                        return string.Empty;
                    if (Value is IList)
                        return ConvertToString((IList)Value);
                    return Convert.ToString(Value);
                }
                return _displayValue;
            }
            set { _displayValue = value; }
        }
        private string _displayValue = null;
        #endregion

        #region ModifiedDisplayValue
        [XmlIgnore]
        public string ModifiedDisplayValue
        {
            get
            {
                if (_modifiedDisplayValue == null)
                {
                    if (ModifiedValue == null)
                        return string.Empty;
                    if (ModifiedValue is IList)
                        return ConvertToString((IList)ModifiedValue);
                    return Convert.ToString(ModifiedValue);
                }
                return _modifiedDisplayValue;
            }
            set { _modifiedDisplayValue = value; }
        }
        private string _modifiedDisplayValue = null;
        #endregion

        #region Archival values
        [XmlElement("listValue")]
        public List<int> SerialisableValueList { get; set; }

        [XmlElement("value")]
        public object SerialisableValue { get; set; }

        #region private helpers
        private object GetValue()
        {
            if (SerialisableValueList != null && DataType != null && DataType.ToLower().Contains("list"))
            {
                return SerialisableValueList;
            }
            return SerialisableValue;
        }

	    private string ConvertToString(IList values)
	    {
		    string result = string.Empty;
		    if (values == null) return string.Empty;
		    foreach (var val in values)
		    {
			    result += Convert.ToString(val) + ", ";
		    }
		    if (result.Length > 2) result = result.Substring(0, result.Length - 2);
		    if (result.Length > 200) result = result.Substring(0, 200);
		    return result;
		}

        private void SetValue(object value)
        {
            if (value is List<int>) SerialisableValueList = (List<int>)value;
            else if (value is DBNull) SerialisableValue = null;
            else SerialisableValue = value;
        }
        #endregion
        #endregion

        [XmlIgnore]
        public bool ModifiedByBRE { get; set; }

        //[XmlAttribute("filterFunction")]
        //public FilterFunctionEnum FilterFunction { get; set; }

        [XmlAttribute("entitySearchMatchTypes")]
        public string EntitySearchMatchTypes { get; set; }

        /// <summary>
        /// Used for bulk file processing to mark how are lookup fields sent
        /// Value : as fenergo value
        /// Text : as exact fenergo text
        /// Both : as Value or Text
        /// </summary>
        [XmlAttribute("importLookupType")]
        public string ImportLookupType { get; set; }

        /// <summary>
        /// Used for data migration 
        /// Value : Normal Ignore
        /// Default vallue as normal
        /// If ignore then field is ignored by XSD generator
        /// </summary>
        [XmlAttribute("migration")]
        public string Migration { get; set; }

        /// <summary>
        /// Field's value is not sent over the network when set to 'true'.
        /// Example: field contains sensitive data which should be masked if user doesn't have required permissions,
        ///          but it is used for BRE calculations. We need two fields for such scenario: one for displaying 
        ///          sensitive data on UI and another one for internal use only which should not be passed over the network.
        /// </summary>
        [XmlAttribute("forInternalUseOnly")]
        public bool ForInternalUseOnly { get; set; }

        [XmlAttribute("disableAudit")]
        public bool DisableAudit { get; set; }

        [XmlAttribute("sensitiveDataPermission")]
        public string SensitiveDataPermission { get; set; }

        [XmlAttribute("sensitiveDataDependencies")]
        public string SensitiveDataDependencies { get; set; }

        [XmlAttribute("mask")]
        public string Mask { get; set; }

        [XmlAttribute("source")]
        public string Source { get; set; }

        public override string ToString()
        {
            return $"Key:{Key}, Source:{SourceFieldName}, Value:{Value}, Modified:{ModifiedValue}";
        }
    }
}
