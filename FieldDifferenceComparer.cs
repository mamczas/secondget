﻿using System.Collections.Generic;

namespace SecondGetParser
{
    internal class FieldDifferenceComparer : EqualityComparer<FieldDifference>
    {
        public override bool Equals(FieldDifference x, FieldDifference y)
        {
            if (x is null || y is null)
            {
                return false;
            }

            var xString = x.ToString();
            var yString = y.ToString();

            return xString.Equals(yString);
        }

        public override int GetHashCode(FieldDifference fieldDifference)
        {
            return fieldDifference is null ? 0 : fieldDifference.GetHashCode();
        }
    }
}
