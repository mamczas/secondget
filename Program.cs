﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace SecondGetParser
{
    internal class Program
    {
        private static int Count = 0;

        private static string NextLine(IEnumerator<string> sr)
        {
            sr.MoveNext();
            Count++;
            return sr.Current;
        }

        static void Main(string[] args)
        {
            var listOfDifferences = new ListOfDifferences();

            {
                foreach (var file in Directory.EnumerateFiles(@"d:\SRC\SGet\Second_Get\", "*.log"))
                {
                    var sr = File.ReadLines(file).GetEnumerator();

                    while (true)
                    {
                        // Read the stream to a string, and write the string to the console.
                        var line = NextLine(sr);
                        var diff = new Difference();
                        if (line == null)
                        {
                            Count = 0;
                            goto tutej;
                        }

                        while (!line.Contains("HttpContext-Body"))
                        {
                            line = NextLine(sr);
                            if (line == null)
                            {
                                Count = 0;
                                goto tutej;
                            }
                        }

                        var requestContext = Regex.Match(line, ".*requestContext\\\":\\\"([\\w\\-]+)",
                            RegexOptions.IgnoreCase);
                        if (requestContext.Success)
                        {
                            diff.ContextName = requestContext.Groups[1].Value;
                        }

                        while (!line.Contains("[Response:["))
                        {
                            line = NextLine(sr);
                        }

                        var response = new List<EntityDataSourceDTO>();
                        var datasourcelist = Regex.Match(line, "\\[Response:(.*)\\]", RegexOptions.IgnoreCase);
                        if (datasourcelist.Success)
                        {
                            response = JsonConvert.DeserializeObject<List<EntityDataSourceDTO>>(datasourcelist.Groups[1]
                                .Value);
                        }

                        line = NextLine(sr);
                        if (line == null || !line.Contains("[ResGetDataSources:"))
                        {
                            break;
                        }

                        var responsewithGet = new List<EntityDataSourceDTO>();
                        var datasourcelistwithget =
                            Regex.Match(line, "\\[ResGetDataSources:(.*)\\]", RegexOptions.IgnoreCase);
                        if (datasourcelistwithget.Success)
                        {
                            responsewithGet =
                                JsonConvert.DeserializeObject<List<EntityDataSourceDTO>>(datasourcelistwithget.Groups[1]
                                    .Value);
                        }

                        var (dsDifferences, fieldDifferences, valueDifferences, fieldsDifferencesList) = CompareDataSources(response, responsewithGet);

                        if (!string.IsNullOrEmpty(dsDifferences))
                        {
                            diff.DatasourceName = dsDifferences;
                            diff.Fields = fieldDifferences;
                            diff.AdditionalInfo = JsonConvert.SerializeObject(fieldsDifferencesList).Replace("\\\"", "'");
                        }

                        listOfDifferences.AddDifference(diff);
                    }
                    tutej:
                    Console.WriteLine(file);
                }
            }

            var csv = new StringBuilder();
            csv.AppendLine("sep=\\");
            csv.AppendLine("ContextName\\DataSourceName\\Fields\\Count\\AdditionalInfo");

            var unique = new List<string>();
            foreach (var diff in listOfDifferences.differences)
            {
                if (!string.IsNullOrEmpty(diff.Fields))
                {
                    unique.AddRange(diff.Fields.Split(','));
                }

                csv.AppendLine(
                    $"{diff.ContextName}\\{diff.DatasourceName}\\{diff.Fields}\\{diff.Count}\\{diff.AdditionalInfo ?? string.Empty}");
            }

            csv.AppendLine(string.Join(",", unique.Distinct()));

            File.WriteAllText("results.csv", csv.ToString());
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        static (string dsDifferences, string fieldDifferences, string valueDifferences, List<FieldDifference>
            fieldsDifferencesList) CompareDataSources(List<EntityDataSourceDTO> withoutGet,
                List<EntityDataSourceDTO> withGet)
        {
            var dsDifferences = new Dictionary<string, DatasourceDifference>();
            foreach (var ds in withoutGet)
            {
                var withGetDS = withGet.SingleOrDefault(x =>
                    x.DataSourceName == ds.DataSourceName &&
                    ds.EntityDataFieldList.Count == x.EntityDataFieldList.Count);
                if (withGetDS == null)
                {
                    dsDifferences.Add(ds.DataSourceName ?? "null",
                        new DatasourceDifference(new List<string> { "no data-source found in withoutGet" },
                            new List<string> { "no data-source found in withoutGet" }, new List<FieldDifference>()));
                }
                else
                {
                    var fields = new List<string>();
                    var values = new List<string>();
                    var fieldDifferences = new List<FieldDifference>();
                    foreach (var field in ds.EntityDataFieldList.Where(x => x.Key.ToLowerInvariant().Contains("rowversion") || x.DataType != "timestamp"))
                    {
                        var withGetField = withGetDS.EntityDataFieldList.Single(x => x.Key == field.Key);
                        var comparisionResult = CompareDataFields(withGetField, field);
                        if (!comparisionResult.isDifferent)
                        {
                            continue;
                        }

                        fields.Add(comparisionResult.fieldDifferences);
                        values.Add(comparisionResult.valueDifferences);
                        fieldDifferences.AddRange(comparisionResult.fieldDifferencesList);
                    }

                    if (fields.Any())
                    {
                        dsDifferences.Add(ds.DataSourceName,
                            new DatasourceDifference(fields, values, fieldDifferences));
                    }
                }
            }

            var fieldsList = new List<FieldDifference>();
            foreach (var key in dsDifferences.Keys)
            {
                fieldsList.AddRange(dsDifferences[key].FieldDifferencesList);
            }

            return (string.Join(",", dsDifferences.Keys),
                string.Join(",", dsDifferences.Values.Select(x => x.DifferentFields).ToList()),
                string.Join(",", dsDifferences.Values.Select(x => x.DifferenceValues).ToList()), fieldsList);
        }

        /// <summary>
        /// The compare data fields.
        /// </summary>
        /// <param name="withoutGet">
        /// The without get.
        /// </param>
        /// <param name="withGet">
        /// The with get.
        /// </param>
        /// <returns>
        /// The <see cref="(bool, string, string)"/>.
        /// </returns>
        public static (bool isDifferent, string fieldDifferences, string valueDifferences, List<FieldDifference>
            fieldDifferencesList) CompareDataFields(EntityDataFieldDTO withoutGet, EntityDataFieldDTO withGet)
        {
            var differences = new Dictionary<string, FieldDifference>();

            var valueWithoutGet = GetValueOfPropertyOrNull(withoutGet.ModifiedValue ?? withoutGet.Value);
            var valueWithGet = GetValueOfPropertyOrNull(withGet.ModifiedValue ?? withGet.Value);


            if (valueWithoutGet.Equals(valueWithGet))
            {
                return (false, string.Empty, string.Empty, new List<FieldDifference>());
            }

            if (GetValueOfPropertyOrNull(withoutGet.ModifiedValue) != GetValueOfPropertyOrNull(withGet.ModifiedValue))
            {
                var field = new FieldDifference(withoutGet.Key, GetValueOfPropertyOrNull(valueWithGet), GetValueOfPropertyOrNull(valueWithoutGet))
                {
                    ModifiedValueWithGet = GetValueOfPropertyOrNull(withGet.ModifiedValue),
                    ModifiedValueWithoutGet = GetValueOfPropertyOrNull(withoutGet.ModifiedValue),
                    ValueWithGet = GetValueOfPropertyOrNull(withGet.Value),
                    ValueWithoutGet = GetValueOfPropertyOrNull(withoutGet.Value),
                    FieldName = nameof(withGet.ModifiedValue)
                };

                differences.Add(nameof(withGet.ModifiedValue), field);
            }

            if (GetValueOfPropertyOrNull(withoutGet.Value) != GetValueOfPropertyOrNull(withGet.Value))
            {
                var field = new FieldDifference(withoutGet.Key, GetValueOfPropertyOrNull(valueWithGet),
                    GetValueOfPropertyOrNull(valueWithGet))
                {
                    ModifiedValueWithGet = GetValueOfPropertyOrNull(withGet.ModifiedValue),
                    ModifiedValueWithoutGet = GetValueOfPropertyOrNull(withoutGet.ModifiedValue),
                    ValueWithGet = GetValueOfPropertyOrNull(withGet.Value),
                    ValueWithoutGet = GetValueOfPropertyOrNull(withoutGet.Value),
                    FieldName = nameof(withGet.Value)
                };

                differences.Add(nameof(withGet.Value), field);
            }

            return (isDifferent: differences.Any(), 
                fieldDifferences: string.Join(",", differences.Keys),
                valueDifferences: string.Join("\\", differences.Values),
                fieldDifferencesList: differences.Values.ToList());
        }

        private static string GetValueOfPropertyOrNull(object value)
        {
            return value?.ToString() ?? "null";
        }
    }
}
